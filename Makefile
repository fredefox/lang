src/Makefile: grammer.cf
	bnfc $< -p Language.Lang -o src -m

src/Language/Lang/TestGrammer: ./src/Makefile
	cd src && make all

.PHONY: install parse

install: src/Language/Lang/TestGrammer
	install $< ~/.local/bin/lang-parse

parse: install
	lang-parse lang/small.lang
